﻿using System;
namespace NotebookApp
{
    public class Note
    {
        public Note(string name, string surname, string country, ulong phoneNumber, string thirdname = null, string birthdate = null, string company = null, string position = null, string otherNotes = null)
        {
            this.name = name;
            this.surname = surname;
            this.country = country;
            this.phoneNumber = phoneNumber;
            this.id = idcounter++;
            this.thirdname = thirdname;
            this.birthdate = birthdate;
            this.company = company;
            this.position = position;
            this.otherNotes = otherNotes;
        }
        public string surname;
        public string name;
        public string thirdname;
        public ulong phoneNumber;
        public string country;
        public string birthdate;
        public string company;
        public string position;
        public string otherNotes;
        public int id;
        private static int idcounter = 0;

        public override string ToString()
        {
            return $"Имя: {name}\nФамилия: {surname}\nОтчество: {thirdname}\n" +
                $"Номер телефона: {phoneNumber}\nСтрана: {country}\nДата рождения: {birthdate}\nОрганизация: {company}\n" +
                $"Должность: {position}\nПрочие заметки: {otherNotes}";
        }

    }
    
}
