﻿using System;
using System.Collections.Generic;

namespace NotebookApp
{
    class Program
    {
        static List<Note> notes = new List<Note>();

        static string[] menuOptions =
        {
            "Создание новой записи",
            "Редактирование записей",
            "Удаление записей",
            "Просмотр записи",
            "Просмотр всех записей",
            "Выход"
        };
        public static void ShowOptions(string[] options)
        {
            Console.WriteLine("Доступные действия:");
            for (int i=1;i<options.Length+1; i++)
            {
                Console.WriteLine($"{i}. {options[i-1]}");
            }
        }
        static void Main(string[] args)
        {

            bool isWorking = true;
            while (isWorking)
            {
                ShowOptions(menuOptions);
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        createNewNote();
                        break;
                    case "2":
                        editNote();
                        break;
                    case "3":
                        deleteNote();
                        break;
                    case "4":
                        showNote();
                        break;
                    case "5":
                        showAllNotes();
                        break;
                    case "6":
                        isWorking = false;
                        break;
                    default:
                        Console.WriteLine("Неправильный ввод");
                        break;

                }
            }
            
        }

        private static void showAllNotes()
        {
            Console.WriteLine("Записи:");
            if (notes.Count == 0)
            {
                Console.WriteLine("Нет записей");
            }
            foreach(var note in notes)
            {
                Console.WriteLine($"Id: {note.id}\nФамилия: {note.surname}\nИмя: {note.name}\nНомер телефона: {note.phoneNumber}");
                Console.WriteLine("---------------------------------");
            }
        }

        private static void showNote()
        {
           
            int id = getId();
            Note ids = notes.Find(note => note.id == id);
            if (ids == null)
            {
                Console.WriteLine("Запись не найдена");
                return;
            }
            Console.WriteLine(ids.ToString());
        }

        private static int getId()
        {
            int Id;
            Console.Write("Введите Id: ");
            while (!int.TryParse(Console.ReadLine(), out Id))
            {
                Console.Write("Id введен некорректно, введите Id: ");
            }
            return Id;
        }

        private static void deleteNote()
        {
            
            int id = getId();
            Note ids = notes.Find(note => note.id == id);
            if (ids == null)
            {
                Console.WriteLine("Запись не найдена");
                return;
            }
            notes.Remove(ids);

        }

        private static void editNote()
        {
            
            int id = getId();
            Note ids = notes.Find(note => note.id == id);
            if (ids == null)
            {
                Console.WriteLine("Запись не найдена");
                return;
            }

            ids.surname = getString($"Фамилия ({ids.surname})");

            ids.name = getString($"Имя ({ids.name})");

            Console.Write($"Отчество ({ids.thirdname}): ");
            ids.thirdname = Console.ReadLine();

            ids.phoneNumber = getPhoneNumber(ids.phoneNumber);

            ids.country = getString($"Страна ({ids.country})");

            Console.Write($"Дата рождения ({ids.birthdate}): ");
            ids.birthdate = Console.ReadLine();
            Console.Write($"Организация ({ids.company}): ");
            ids.company = Console.ReadLine();
            Console.Write($"Должность ({ids.position}): ");
            ids.position = Console.ReadLine();
            Console.Write($"Прочие заметки ({ids.otherNotes}): ");
            ids.otherNotes = Console.ReadLine();


        }

        private static void createNewNote()
        {
           
            string surname = getString("Фамилия");
            
            string name = getString("Имя");

            Console.Write("Отчество: ");
            string thirdname = Console.ReadLine();
            
            ulong phoneNumber = getPhoneNumber();

            string country = getString("Страна");

            Console.Write("Дата рождения: ");
            string birthdate = Console.ReadLine();
            Console.Write("Организация: ");
            string company = Console.ReadLine();
            Console.Write("Должность: ");
            string position = Console.ReadLine();
            Console.Write("Прочие заметки: ");
            string otherNotes = Console.ReadLine();

            Note newNote = new Note(name, surname, country, phoneNumber, thirdname, birthdate, company, position, otherNotes);
            notes.Add(newNote);
        }

        private static string getString(string name)
        {
            Console.Write($"{name}: ");
            string input;
            while((input = Console.ReadLine()) == "")
            {
                Console.Write($"Введите {name}: ");
            }
            return input;
        }

        private static ulong getPhoneNumber(ulong phoneNumber = 0)
        {
            if (phoneNumber == 0)
            {
                Console.Write("Номер телефона: ");
            }
            else
            {
                Console.Write($"Номер телефона ({phoneNumber}): ");
            }
            
            while (!ulong.TryParse(Console.ReadLine(), out phoneNumber))
            {
                Console.Write("Номер введен некорректно, введите номер: ");
            }
            return phoneNumber;
        }
    }
}
